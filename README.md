## tf2_client (noetic) - 1.0.0-2

The packages in the `tf2_client` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic tf2_client` on `Tue, 23 May 2023 09:22:22 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.0-1`
- new version: `1.0.0-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## tf2_client (melodic) - 1.0.0-2

The packages in the `tf2_client` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic tf2_client` on `Tue, 23 May 2023 09:19:50 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.0-1`
- new version: `1.0.0-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## tf2_client (noetic) - 1.0.0-1

The packages in the `tf2_client` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic tf2_client` on `Thu, 11 May 2023 17:07:30 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.0.0-1`
- new version: `1.0.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## tf2_client (melodic) - 1.0.0-1

The packages in the `tf2_client` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic tf2_client` on `Thu, 11 May 2023 17:04:48 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `0.0.0-1`
- new version: `1.0.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## tf2_client (noetic) - 0.0.0-1

The packages in the `tf2_client` repository were released into the `noetic` distro by running `/usr/bin/bloom-release --track noetic -r noetic --new-track tf2_client` on `Wed, 10 May 2023 11:35:20 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.0.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## tf2_client (melodic) - 0.0.0-1

The packages in the `tf2_client` repository were released into the `melodic` distro by running `/usr/bin/bloom-release --track melodic -r melodic --new-track tf2_client` on `Wed, 10 May 2023 11:30:29 -0000`

The `tf2_client` package was released.

Version of package(s) in repository `tf2_client`:

- upstream repository: https://github.com/tpet/tf2_client.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `0.0.0-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


